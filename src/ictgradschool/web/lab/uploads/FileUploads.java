package ictgradschool.web.lab.uploads;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class FileUploads extends HttpServlet {

    private File uploadsFolder;
    private File tempFolder;



    //TODO - make this a proper HttpServlet that uses init() and doPost() methods

    @Override
    public void init() throws ServletException {
        super.init();
        //Get the upload folder, ensure it exists
        this.uploadsFolder = new File(getServletContext().getRealPath("/Uploaded-Photos"));
        if (!uploadsFolder.exists()) {
            uploadsFolder.mkdirs();
        }
        //create the temporary folder that the file-upload mechanism needs.
        this.tempFolder = new File(getServletContext().getRealPath("/WEB-INF/temp"));
        if (!tempFolder.exists()) {
            tempFolder.mkdirs();
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setSizeThreshold(4 * 1024);
        factory.setRepository(tempFolder);
        ServletFileUpload upload = new ServletFileUpload(factory);

        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();

        //try was for other things in servlet by Andrew so should work without but will leave it in
        try {
            List<FileItem> fileItems = upload.parseRequest(req);
            //create a file object here so that we can write an image to it on the server later
            File fullsizeImageFile = null;

            for (FileItem fi : fileItems) {
                if (!fi.isFormField() && (fi.getContentType().equals("image/png")) || fi.getContentType().equals("image/jpeg")) {
                    String fileName = fi.getName();
                    fullsizeImageFile = new File(uploadsFolder, fileName);
                    fi.write(fullsizeImageFile);
                }

            }
            File [] allImages = uploadsFolder.listFiles();
            for (File img : allImages) {
                out.println("<img src=\"../Uploaded-Photos/" + img.getName() + "\" width=\"200\">");

           }
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    }
//out.println("<img src=\"../Uploaded-Photos/" + fullsizeImageFile.getName() + "\" width=\"200\">");
//use this line if not using the for loop
